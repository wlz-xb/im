![Image text](https://gitee.com/qq78980809/im/raw/master/src/main/resources/page/image/jt.png)
# 说明
本项目基于tio-websocket-showcase进行改造

#### 项目介绍
1. 启动类：org.tio.showcase.websocket.server.ShowcaseWebsocketStarter
2. 修改src\main\resources\page\im.js中的ip地址，默认是127.0.0.1，可以改成自己的局域网IP或者公网IP。
3. 启动后，在浏览器地址栏输入：http://{ip}/im.html即可，其中ip为你修改后的IP地址，端口号默认采用80端口

#### 安装教程

1、本项目采用maven构建。

